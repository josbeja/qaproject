@formulario4
Feature: Validación de datos adicionales
  
  Background: Pasos previos
    Given Un Postulante carga a la web Credito continuidad de estudios
    When El Postulante ingresa con el DNI y contraseña validos
    Then Postulante ingreso satisfactoriamente
    And Lleno los datos generales satisfactoriamente
    And Lleno los datos de los padres satisfactoriamente 
    And Lleno los datos de contacto satisfactoriamente
    And Cargo satisfactoriamente el formulario de datos adicionales

  Scenario: Autocargado con datos de formularios anteriores
    Given Formulario con datos de padre y madre
    Then Agregamos nuevo miembro
    And Registramos el aporte mensual de cada miembro