@formulario2
Feature: Validación de datos de tus padres
  Muestra datos precargados de RENIEC
  
  Background: Pasos previos
    Given Un Postulante carga a la web Credito continuidad de estudios
    When El Postulante ingresa con el DNI y contraseña validos
    Then Postulante ingreso satisfactoriamente
    And Lleno los datos generales satisfactoriamente
  
  Scenario: Validación de campos del formulario "Datos de padres"
      Given El formulario cargo satisfactoriamente
      Then Validar que existan los siguientes datos de los padres del Postulante
        |Parentesto|
        |Padre|
        |Madre|

  Scenario: Verificar que se muestre el boton editar los datos de los padres
    Given El formulario cargo satisfactoriamente
    Then Validar que exista el boton editar los datos de los padres

  Scenario: Verificar que se pueda editar los datos de los padres
    Given El formulario cargo satisfactoriamente
    Then Validar que exista el boton editar los datos de los padres
    And Click en el boton editar datos del padre
    Then Ingresar DNI del padre 
    And Dar click en la lupa de busqueda
    Then Click en el boton confirmar 
    And Validar que los datos se hayan cargado exitosamente

  Scenario: Para postulante menor de edad, datos de padres no autocargados
    Given Formulario cargado satisfactoriamente
    When Validamos que no existen datos de padres precargados
    Then El sistema no permite continuar sin cargar informacion de almenos un padre

  Scenario: Para mayor de edad, datos de padres no autocargados
    Given Formulario cargado satisfactoriamente
    When Validamos que no existen datos de padres precargados
    Then El sistema permite continuar sin cargar informacion de los padres
 