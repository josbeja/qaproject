@formulario1
Feature: Auto carga de datos generales
  Verificación de datos generales
  
  Background: Pasos previos
    Given Un Postulante carga a la web Credito continuidad de estudios
    When El Postulante ingresa con el DNI y contraseña validos
    Then Postulante ingreso satisfactoriamente

#Con errores
  Scenario: Validación de campos del formulario "Datos Generales"
    Given Datos generales cargados satisfactoriamente
    Then Validar que existan los siguientes datos del Postulante:
      |Datos del Postulante|
      |Documento de identidad|
      |Apellidos y nombres|
      |Fecha de nacimiento|
      |Sexo|
      |Edad|
      |Domicilio RENIEC|
      |Condición socioeconómica|
      #no existen antecedentes
      |Antecedentes|
      |Lengua Materna|
      #campo adicional antepazados no contemplados en el requerimiento
      |Discapacidad|
      |Tipo de discapacidad|

  Scenario: Validar valor seleccionado en lengua materna
    Given Datos generales cargados satisfactoriamente
    Then Validamos que exista una lengua materna seleccionada
    
  Scenario: Validar valor seleccionado NO en discapacidad
    Given Datos generales cargados satisfactoriamente
    Then Validamos que exista un valor NO seleccionado en discapacidad

  Scenario: Validar tipo de discapacidad
    Given Datos generales cargados satisfactoriamente
    When Seleccionamos valor SI en discapacidad
    Then Seleccionamos tipo de discapacidad de una lista desplegable
    And Click en el boton aceptar
    Then Validamos que el valor seleccionado se actualice

#no se puede validar no hay data disponible para la prueba
  Scenario: Mensaje para postulantes menores de edad
    Given Postulante menor de edad
    Then Se debe mostrar el aviso:
      """
      Usted es menor de edad, por lo tanto, deberá declarar quien será su representante 
      y cargar el documento correspondiente en el paso 3
      """

#Fail: no cumple con el requerimiento
  Scenario: Antecedentes salen como Observado
    Given En la seccion antecedentes se muestra el mensaje "Observado"
    Then Se muestra el mensaje
      """
      Si en antecedentes sale el estado de OBSERVADO en el paso 3 (cargar documentos) 
      se te solicitará adjuntes el certificado respectivo
      """

#Fail: no aparece el mensaje DNI 70196786
  Scenario: Condición socioeconomica de no ser pobre o pobre extremo validado por SISFOH
    Given Postulante no pobre o pobre extremo
    Then Se muestra el mensaje
      """
      Ud no está catalogado como POBRE o POBRE EXTREMO en el registro del sistema de 
      Focalización de Hogares (SISFOH). Deberá acreditar si tiene condición de 
      vulnerabilidad económica (si tu hogar es receptor de alguno de los bonos 
      monetarios desplegados por la Emergencia Sanitaria: Bono Independiente o 
      Bono Familiar Universal. Debes cargar algún documento de sustento en el 
      paso 3 (Cargar documentos)
      """